//https://developers.google.com/maps/documentation/javascript/places-autocomplete
Drupal.behaviors.google_places_js = {
	attach:function(context, settings){
		jQuery('input.google-places-js-field').each(function(){
			var source_field = jQuery(this);
			var source_field_id = source_field.attr('name'),
				target_parent = source_field.closest('div.form-item');
				
			var source_field_id_root = source_field_id.split('[')[0];
			
			var autocomplete_options = {
				types:[Drupal.settings.google_places_js[source_field_id_root].types]
			};
			if(Drupal.settings.google_places_js[source_field_id_root].country != 'none'){
				autocomplete_options.componentRestrictions = {
					country:Drupal.settings.google_places_js[source_field_id_root].country,
				};
			}
			
			var autocomplete = new google.maps.places.Autocomplete(source_field[0], autocomplete_options);
			
			//http://stackoverflow.com/questions/12816428/how-to-fire-place-changed-event-for-google-places-auto-complete-on-enter-key
			source_field.keypress(function(event){
				if(event.which == 13){
					google.maps.event.trigger(autocomplete, 'place_changed');
					return false;
				}
			});
			
			//http://stackoverflow.com/questions/1481152/how-to-detect-a-textboxs-content-has-changed
			source_field.bind('input', function(){
				var hidden_fields = [
					//these are the unmodified keys from the autocomplete results
					'name',
					'url',
					'place_id',
					'address_components',	//json-encoded
					'vicinity',
					'types',				//json-encoded
					'longitude',
					'latitude',
					'viewport_longitude',
					'viewport_latitude'
				]
				
				for(key in hidden_fields){
					var target_field_id_at = source_field_id.replace('formatted_address', hidden_fields[key]);
					target_parent.siblings('input[name="' + target_field_id_at + '"]').val('');
				}				
				
				source_field.removeClass('validated');
			});
			
			google.maps.event.addListener(autocomplete, 'place_changed', function(){
				var place = autocomplete.getPlace();
				var place_distilled = {
					name:				place.name,
					place_id:			place.place_id,
					types:				JSON.stringify(place.types),
					url:				place.url,
					address_components:	JSON.stringify(place.address_components),
					vicinity:			place.vicinity,
					latitude:			place.geometry.location.lat(),
					longitude:			place.geometry.location.lng(),
				};
				
				if(place.geometry.viewport){
					place_distilled.viewport_latitude = JSON.stringify([place.geometry.viewport.getNorthEast().lat(), place.geometry.viewport.getSouthWest().lat()]);
					place_distilled.viewport_longitude = JSON.stringify([place.geometry.viewport.getNorthEast().lng(), place.geometry.viewport.getSouthWest().lng()]);
				}
				else{
					target_parent.siblings('input[name="' + source_field_id.replace('formatted_address', 'viewport_latitude') + '"]').val('');					
					target_parent.siblings('input[name="' + source_field_id.replace('formatted_address', 'viewport_longitude') + '"]').val('');					
				}
				
				for(key in place_distilled){
					var target_field_id_at = source_field_id.replace('formatted_address', key);
					target_parent.siblings('input[name="' + target_field_id_at + '"]').val(place_distilled[key]);
				}
				
				source_field.addClass('validated');
			});
		});
	}
};