<?php
/**
*  Implements hook_token_info()
*/

function google_places_js_token_info(){
	return array(
		'types'=>array(
			'google-places-js'=>array(
				'name'=>		t('Google Places JS Field'),
				'description'=>	t('Tokens related to this Google Places JS field'),
				'field'=>		true,
				'needs-data'=>	'google-places-js'
			),
		),
		'tokens'=>array(
			'google-places-js'=>_google_places_js_supported_tokens(),
		),
	);
}

/**
 * Implements hook_token_info_alter().
 */
function google_places_js_token_info_alter(&$data) {
	foreach(array_filter(field_info_field_map(), '_google_places_js_map_filter') as $field_name=>$field) {
		foreach ($data['tokens'] as $group => $token){
			if (isset($data['tokens'][$group][$field_name]) && is_array($data['tokens'][$group][$field_name])) {
				$data['tokens'][$group][$field_name]['type'] = 'google-places-js';
			}
		}
	}
}

function _google_places_js_supported_tokens(){
	return array(
		'formatted_address'=>array(
			'name'=>		'Formatted Address',
			'description'=>	'Formatted address of the location',	
		),
		'name'=>array(
			'name'=>		'Name',
			'description'=>	'Entered name of the location',	
		),
		'url'=>array(
			'name'=>		'Google Places URL',
			'description'=>	'Google Places URL of the location',	
		),
		'place_id'=>array(
			'name'=>		'Google Places ID',
			'description'=>	'Google Places ID of the location',	
		),
		'component_country_long'=>array(
			'name'=>		'Country (Long)',
			'description'=>	'Country of the location',	
		),
		'component_country_short'=>array(
			'name'=>		'Country (Short)',
			'description'=>	'Two-letter ISO code for the country of the location',	
		),		
		'component_administrative_area_level1'=>array(
			'name'=>		'Administrative Area',
			'description'=>	'Administrative area of the location (e.g. state, province, region)',	
		),
		'component_administrative_area_level1_short'=>array(
			'name'=>		'Administrative Area (Short)',
			'description'=>	'Abbreviated name for the administrative area of the location (e.g. state, province, region)',	
		),		
		'component_administrative_area_level2'=>array(
			'name'=>		'Sub-administrative Area',
			'description'=>	'Administrative area of the location (e.g. country, district)',		
		),
		'component_administrative_area_level2_short'=>array(
			'name'=>		'Sub-administrative Area (Short)',
			'description'=>	'Abbreviated name for the administrative area of the location (e.g. country, district)',		
		),		
		'component_locality'=>array(
			'name'=>		'Locality',
			'description'=>	'Locality of the location (e.g. city, town)',	
		),
		'component_postal_code'=>array(
			'name'=>		'Postal Code/ZIP code',
			'description'=>	'Postal Code/Zip code of the location',	
		),
		'vicinity'=>array(
			'name'=>		'Vicinity',
			'description'=>	'Vicinity definition of the location',	
		),
/*		'types'=>array(
			'name'=>		'Place Type(s)',
			'description'=>	'Comma-separated place type(s) of the location',	
		),
		'viewport_latitude_ne'=>array(
			'name'=>		'Formatted Address',
			'description'=>	'Formatted address of the location',	
		),
		'viewport_longitude_ne'=>array(
			'name'=>		'Formatted Address',
			'description'=>	'Formatted address of the location',	
		),
		'viewport_latitude_sw'=>array(
			'name'=>		'Formatted Address',
			'description'=>	'Formatted address of the location',	
		),
		'viewport_longitude_sw'=>array(
			'name'=>		'Formatted Address',
			'description'=>	'Formatted address of the location',	
		),*/
		'latitude'=>array(
			'name'=>		'Latitude',
			'description'=>	'Latitude of the location',	
		),
		'longitude'=>array(
			'name'=>		'Longitude',
			'description'=>	'Longitude of the location',	
		),
	);
}

function google_places_js_tokens($type, $tokens, $data = array(), $options = array()){
	$replacements = array();
	$language_code = (isset($options['language']) ? $options['language']->language : LANGUAGE_NONE);
	$sanitize = !empty($options['sanitize']);
	
	if($type == 'entity'){
		foreach(array_filter(field_info_field_map(), '_google_places_js_map_filter') as $field_name=>$field) {
			if($google_places_js_tokens = token_find_with_prefix($tokens, $field_name)){
				$replacements += token_generate('google-places-js', $google_places_js_tokens, array('google-places-js' => $data['entity']->$field_name), $options);
			}
		}
	}	
	
	if($type == 'google-places-js' && !empty($data[$type][$language_code]) && is_array($data[$type][$language_code])){
		$field = $data[$type][$language_code];
		$field = $field[0];
		
		foreach($tokens as $name=>$original){
			switch($name){
				case 'formatted_address':
				case 'name':
				case 'url':
				case 'place_id':
				case 'vicinity':
				case 'latitude':
				case 'longitude':
					$replacements[$original] = ($sanitize ? check_plain($field[$name]) : $field[$name]);
					break;								
				case 'component_country_long':
					$value = _google_places_js_parse_address_components($field['address_components'], 'country');
				case 'component_country_short':
					if(!isset($value)){
						$value = _google_places_js_parse_address_components($field['address_components'], 'country', 'short_name');
					}
				case 'component_administrative_area_level1':
					if(!isset($value)){				
						$value = _google_places_js_parse_address_components($field['address_components'], 'administrative_area_level_1');
					}
				case 'component_administrative_area_level1_short':
					if(!isset($value)){
						$value = _google_places_js_parse_address_components($field['address_components'], 'administrative_area_level_1', 'short_name');
					}
				case 'component_administrative_area_level2':
					if(!isset($value)){				
						$value = _google_places_js_parse_address_components($field['address_components'], 'administrative_area_level_2');
					}
				case 'component_administrative_area_level2_short':
					if(!isset($value)){				
						$value = _google_places_js_parse_address_components($field['address_components'], 'administrative_area_level_2', 'short_name');
					}					
				case 'component_locality':					
					if(!isset($value)){				
						$value = _google_places_js_parse_address_components($field['address_components'], 'locality');
					}
				case 'component_postal_code':
					if(!isset($value)){				
						$value = _google_places_js_parse_address_components($field['address_components'], 'postal_code');
					}
					$replacements[$original] = ($sanitize ? check_plain($value) : $value);																									
					unset($value);
					break;	
			}		
		}
	}
	
	return $replacements;
}


// !Utility Functions
function _google_places_js_parse_address_components($address_components, $place_type, $size_specifier = 'long_name'){
	$address_components = json_decode($address_components);
	foreach($address_components as $address_component){
		if(in_array($place_type, $address_component->types)){
			return $address_component->$size_specifier;
		}
	}
	
	return '';
}

function _google_places_js_map_filter($field) {
  return !empty($field['type']) && $field['type'] == 'google_places_js_field';
}